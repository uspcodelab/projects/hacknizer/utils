#!/bin/bash

REPOS=(api-gateway auth-api auth-sink hackathon-api hackathon-sink pwa chart natschart integration-tests)

echo "This script will clone repos using the SSH protocol"
echo "You'll need to configure your public key at GitLab"

cd ..

for r in ${REPOS[*]}; do
  git clone git@gitlab.com:uspcodelab/projects/hacknizer/$r.git
done
