# README

This repo houses some utility scripts, such:

- `clone_all.sh` for cloning all other working repos
- `Makefile` to automate the task of building and upping all working services
- `*.yml` that are docker-compose YAML files for the services

> note that "services" refers only to back-end -- front-end (`pwa`) must be handled manually

