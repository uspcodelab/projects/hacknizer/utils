TIER1=nats-bds.yml
TIER2=apis-sinks.yml
TIER3=gate.yml

NAMESPACE=hacknizer
NATS=nats
AUTH=auth-api
HACK=hackathon-api
GATE=api-gateway


## DOCKER-COMPOSE

compose:
	docker-compose -f ${TIER1} up --build -d
	@sleep 15
	docker-compose -f ${TIER2} up --build -d
	@sleep 15
	docker-compose -f ${TIER2} exec auth npm run db:setup
	docker-compose -f ${TIER2} exec hack npm run db:setup
	docker-compose -f ${TIER3} up --build -d

comp-test-dbs: compose
	# set test databases
	docker-compose -f ${TIER2} exec auth npm run db:setup:test
	docker-compose -f ${TIER2} exec hack npm run db:setup:test

comp-test: comp-test-dbs test-only
	@echo "Test"

test-only:
	# run tests
	docker-compose -f ${TIER2} exec auth npm run test
	docker-compose -f ${TIER2} exec hack npm run test

comp-test-integration: comp-test-dbs
	npm run --prefix ../integration-tests/ test

clean:
	docker-compose -f ${TIER3} down -v --remove-orphans \
		|| docker-compose -f ${TIER2} down -v --remove-orphans \
		|| docker-compose -f ${TIER1} down -v --remove-orphans
	docker volume prune -f


## MINIKUBE+HELM

kube-init:
	@echo "Initializing Minikube"
	@minikube start --vm-driver kvm2
	@echo "Minikube started!"
	@echo "Initializing Helm..."
	@helm init
	@echo "Helm started!"
	@echo "Check is all pods are correctly running..."

kube:
	@helm install -n ${NATS} --namespace ${NAMESPACE} ../natschart
	@echo "Nats deployed!"
	@helm install -n ${AUTH} ../chart \
		--namespace ${NAMESPACE} \
		--values ../${AUTH}/values.yaml
	@echo "Auth deployed!"
	@helm install -n ${HACK} ../chart \
		--namespace ${NAMESPACE} \
		--values ../${HACK}/values.yaml
	@echo "Hack deployed!"
	@echo "Before deploying the Gateway,"
	@echo "verify if all services all running properly"

gate:
	@helm install -n ${GATE} ../chart \
		--namespace ${NAMESPACE} \
		--values ../${GATE}/values.yaml
	@echo "Gateway deplyed!"

